from matplotlib.pyplot import title
from process.fourier_analysis import FourierAnalysis
from process.load_record_wave import LoadWave
from process.record_sound import RecordSound
from process.encode_digital import EncodeSignal
from constants import *

from tkinter import *    
from tkinter import ttk

class Aplicacion():
    
    signal=[]

    def __init__(self):
        raiz = Tk()
        s = ttk.Style()
        s.configure(
            "MyButton.TButton",
            background="#000000",
            padding=2,
            font=("Century Gothic", 12)
        )
        raiz.geometry('1000x400')
        raiz.title('Aplicación')
        title = ttk.Label(raiz, text="Final Project", font=("Century Gothic",35))
        title.pack(pady=20)
        logo = PhotoImage(file="logo (1).png")
        ttk.Label(raiz, image=logo).place(x=250, y=10)
        exit = ttk.Button(raiz, text='Exit', style="MyButton.TButton",command=raiz.destroy)
        exit.place(x=450, y=300)
        ttk.Button(raiz, text='Record', style="MyButton.TButton",command=RecordSound.record).place(x=40, y=100)
        ttk.Button(raiz, text='Load Wave', style="MyButton.TButton",command=self.load_wave).place(x=40, y=150)
        ttk.Button(raiz, text='Fourier analysis', style="MyButton.TButton",command=self.perfom_a).place(x=40, y=200)
        ttk.Button(raiz, text='Digital to digital encode', style="MyButton.TButton",command=self.digital_to_digital).place(x=40, y=250)
        raiz.mainloop()

    def load_wave(self):
        self.signal = LoadWave.load_sound_wave()

    
    def perfom_a(self):
        signal_composite = self.signal
        sampling_rate = SAMPLING_RATE
        time_domain_title = "RECORDED SOUNDS"
        object = FourierAnalysis()
        object.calc_and_plot_xcorr_dft_with_ground_truth(signal_composite,sampling_rate,time_domain_graph_title=time_domain_title)
    
    def digital_to_digital(self):  
       object = EncodeSignal()
       object.encode_signal(self.signal)



        

def main():
    mi_app = Aplicacion()
    return 0

if __name__ == '__main__':
    main()