from constants import *
import scipy.io.wavfile as waves
import IPython
from IPython.lib.display import Audio
import matplotlib.pyplot as plt
import numpy as np

class LoadWave:

    def load_sound_wave():
        
        filename = WAVE_OUTPUT_FILENAME  # cargar archivo de sonido

        rate, signal = waves.read(filename) # Leer el archivo de audio del directorio

        Audio_m = signal[:-800, 0]  
        L = len(Audio_m)  # Longitud de la señal
        # Definir un vector tiempo de la misma longitud de la señal
        n = np.arange(0, L)/rate

        plt.plot(n, Audio_m)
        plt.show()

        IPython.display.Audio(filename)

        return Audio_m
