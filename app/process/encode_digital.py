
import matplotlib.pyplot as plt
import numpy as np
import math

class EncodeSignal:

    BITS = 5
    VOLTAGE = 10
    L = 32 # number of digital samples per data bit
    data = []

    def __init__(self):
        self.RESOLUTION = self.VOLTAGE/(math.pow(2, self.BITS))

    def encode_signal(self, signal):
        analogic=[]
        for value in signal:
            if value >= 0:
                analogic.append(value)
            else:
                analogic.append(0)

        digital = [np.around(a/self.RESOLUTION) for a in analogic[100:500]]
        clk_seq,data_seq,manchester_signal_seq = self.digital_to_digital(digital)
        
        fig, ax = plt.subplots(3,1,sharex='col', figsize=(20, 14))  
        plt.subplots
        ax[0].plot(clk_seq[0:4000]);ax[0].set_title('Clocking')
        ax[1].plot(data_seq[0:4000]);ax[1].set_title('Digital Data')
        ax[2].plot(manchester_signal_seq[0:4000]); ax[2].set_title('Manchester Encoding')
        plt.show()
                 
    def digital_to_digital(self, digital_signal):
        data_repeated = np.repeat(digital_signal,2)
        clk = np.arange(0,2*len(data_repeated)) % 2 # clock samples
        manchester_signal = data_repeated.copy()
        encoded_signal = [] 

        for index,byte in enumerate(manchester_signal):
            if (byte and not clk[index]) or (not byte and clk[index]):
                encoded_signal.append(self.VOLTAGE)
            else:
                encoded_signal.append(0)

        manchester_signal_seq = np.repeat(encoded_signal,2*self.L)
        clk_seq = np.repeat(clk, self.L)
        data_seq = np.repeat(digital_signal,2*self.L)

        return clk_seq, data_seq,manchester_signal_seq
       
