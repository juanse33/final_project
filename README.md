## Instalation of project

Warning: The development was done on windows!!

### Install Virtual Environment and activate it
```sh
$ virtualenv -p python3 venv
```
```sh
$ {route_in_your_pc}/venv/Scripts/Activate.ps1
```
### Install requirements
```sh
$ pip install pipwin
```
```sh
$ python -m pipwin install pyaudio
```
```sh
$ pip3 install -r requirements.txt
```
